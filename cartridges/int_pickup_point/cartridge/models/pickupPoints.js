'use strict';

var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
var PickupPointModel = require('*/cartridge/models/pickupPoint');

/**
 * Creates an array of objects containing pickup point information
 * @param {dw.util.Set} pickupPointsObject - a set of <dw.catalog.Store> objects
 * @returns {Array} an array of objects that contains pickup point information
 */
function createpickupPointsObject(pickupPointsObject) {
    return Object.keys(pickupPointsObject).map(function (key) {
        var pickupPoint = pickupPointsObject[key];
        var pickupPointModel = new PickupPointModel(pickupPoint);
        return pickupPointModel;
    });
}

/**
 * creates pickup point result html
 * @param {Array} pickupPointsArr - array of pickup points
 * @returns {string} html string
 */
function createpickupPointsResultsHtml(pickupPointsArr) {
    return renderTemplateHelper.getRenderedHtml({
        pickupPoints: pickupPointsArr
    }, 'pickupPoint/pickupPointResults');
}

/**
 * @constructor
 * @classdesc The pickup points model
 * @param {dw.util.Set} pickupPointsResultsObject - a set of <dw.catalog.Store> objects
 */
function PickupPoints(pickupPointsResultsObject) {
    this.pickupPoints = createpickupPointsObject(pickupPointsResultsObject);
    this.pickupPointsResultsHtml = createpickupPointsResultsHtml(this.pickupPoints);
}

module.exports = PickupPoints;
