'use strict';

/**
 * @constructor
 * @classdesc The pickup point response model
 * @param {Object} pickupPointResponse - single pickup point object from service response
 */
function PickupPoint(pickupPointResponse) {
    this.id = pickupPointResponse.id;
    this.name = pickupPointResponse.name;
    this.address1 = pickupPointResponse.address;
    this.postalCode = pickupPointResponse.postalCode;
    this.city = pickupPointResponse.city;
    this.countryCode = pickupPointResponse.countryCode;
    this.latitude = pickupPointResponse.latitude;
    this.longitude = pickupPointResponse.longitude;
    this.openHours = pickupPointResponse.openingHoursEnglish;
}

/**
 * Creates an array of objects containing pickup points information
 * @param {Object} pickupPointsResponse - pickup Points service response
 * @returns {Array} an array of objects that contains pickup points information
 */
function createPickupPointsObject(pickupPointsResponse) {
    var result = [];
    if (pickupPointsResponse && pickupPointsResponse.pickupPoint && pickupPointsResponse.pickupPoint.length) {
        pickupPointsResponse.pickupPoint.forEach(function (pickupPoint) {
            result.push(new PickupPoint(pickupPoint));
        });
    }
    return result;
}

/**
 * @constructor
 * @classdesc The pickup point service response model
 * @param {Object} pickupPointsResponse - pickup Points service response
 */
function pickupPoints(pickupPointsResponse) {
    this.pickupPoints = createPickupPointsObject(pickupPointsResponse);
}

module.exports = pickupPoints;
