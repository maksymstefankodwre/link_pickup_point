'use strict';

/**
 * @constructor
 * @classdesc The pickup point response model
 * @param {dw.catalog.Store} storeObject - store object
 */
function PickupPointModel(storeObject) {
    this.ID = storeObject.ID;
    this.name = storeObject.name;
    this.address1 = storeObject.address1;
    this.city = storeObject.city;
    this.postalCode = storeObject.postalCode;
    this.latitude = storeObject.latitude;
    this.longitude = storeObject.longitude;
    this.countryCode = storeObject.countryCode.value;
    this.pickupPointHours = storeObject.storeHours && storeObject.storeHours.markup;
}

module.exports = PickupPointModel;
