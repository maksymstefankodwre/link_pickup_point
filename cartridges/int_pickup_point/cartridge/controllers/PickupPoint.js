'use strict';

var server = require('server');

server.get('Find', function (req, res, next) {
    var pickupPointHelper = require('*/cartridge/scripts/helpers/pickupPointHelper');
    var postalCode = req.querystring.postalCode;
    while (postalCode && postalCode.length < 5) {
        postalCode = '0' + postalCode;
    }
    var pickupPointsModel = pickupPointHelper.getPickupPoints(postalCode);
    res.json(pickupPointsModel);
    next();
});

server.get('GetById', function (req, res, next) {
    var pickupPointId = req.querystring.id ? req.querystring.id : '';
    var pickupPointHelper = require('*/cartridge/scripts/helpers/pickupPointHelper');
    var pickupPointModel = pickupPointHelper.getPickupPointById(pickupPointId);
    var s = 0;
    res.render('pickupPoint/pickupPointDetails', pickupPointModel);
    next();
});

module.exports = server.exports();
