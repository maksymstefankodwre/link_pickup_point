'use strict';

/**
 * Searches for pickup points
 * @param {string} postalCode - postal code for search
 * @returns {Object} pickup points model object
 * */
function getPickupPoints(postalCode) {
    var StoreMgr = require('dw/catalog/StoreMgr');
    var PickupPointsModel = require('*/cartridge/models/pickupPoints');
    var currentSite = require('dw/system/Site').current;
    var countryCode = currentSite.getCustomPreferenceValue('bringPickupPointCountry');

    // radius unit and value are hardcoded and can be moved into site preferences
    var pickupPointResult = StoreMgr.searchStoresByPostalCode(countryCode, postalCode, 'km', 5, 'custom.storeType={0}', 'BRING_PICKUP_POINT');
    return new PickupPointsModel(pickupPointResult.keySet());
}

/**
 * Returns pickup point detalis
 * @param {string} id - pickup point id
 * @returns {Object} pickup point object
 * */
function getPickupPointById(id) {
    var StoreMgr = require('dw/catalog/StoreMgr');
    var PickupPointModel = require('*/cartridge/models/pickupPoint');
    var pickupPoint = StoreMgr.getStore(id);
    return new PickupPointModel(pickupPoint);
}

module.exports = {
    getPickupPoints: getPickupPoints,
    getPickupPointById: getPickupPointById
};
