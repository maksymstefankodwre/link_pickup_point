'use strict';

var Status = require('dw/system/Status');
var FileWriter = require('dw/io/FileWriter');
var XmlStreamWriter = require('dw/io/XMLStreamWriter');

/**
 * Returns array of all pickup point for current site
 * @returns {Array} array of pickup points
 */
function getPickupPoint() {
    var pickupPointAPI = require('*/cartridge/scripts/pickupPoint/pickupPointAPI');
    return pickupPointAPI.getAllPickupPoint();
}

/**
 * Creates Stores XML file at IMPEX
 * @param {Array} pickupPointArr pickup Points JSON received
 * @param {File} file file
 * @returns {dw.system.Status} status
 */
function createStoresImpexFile(pickupPointArr, file) {
    try {
        var fileWriter = new FileWriter(file, 'UTF-8');
        var xsw = new XmlStreamWriter(fileWriter);
        xsw.writeStartDocument('UTF-8', '1.0');
        xsw.writeStartElement('stores');
        xsw.writeDefaultNamespace('http://www.demandware.com/xml/impex/store/2007-04-30');
        for (var x = 0; x < pickupPointArr.length; x++) {
            var postalCode = pickupPointArr[x].postalCode;
            while (postalCode.length < 5) {
                postalCode = '0' + postalCode;
            }
            xsw.writeStartElement('store');
            xsw.writeAttribute('store-id', pickupPointArr[x].id);
            xsw.writeStartElement('name');
            xsw.writeCharacters(pickupPointArr[x].name);
            xsw.writeEndElement();
            xsw.writeStartElement('address1');
            xsw.writeCharacters(pickupPointArr[x].address1);
            xsw.writeEndElement();
            xsw.writeStartElement('city');
            xsw.writeCharacters(pickupPointArr[x].city);
            xsw.writeEndElement();
            xsw.writeStartElement('postal-code');
            xsw.writeCharacters(postalCode);
            xsw.writeEndElement();
            xsw.writeStartElement('country-code');
            xsw.writeCharacters(pickupPointArr[x].countryCode);
            xsw.writeEndElement();
            xsw.writeStartElement('store-hours');
            xsw.writeCharacters(pickupPointArr[x].openHours);
            xsw.writeEndElement();
            xsw.writeStartElement('latitude');
            xsw.writeCharacters(pickupPointArr[x].latitude);
            xsw.writeEndElement();
            xsw.writeStartElement('longitude');
            xsw.writeCharacters(pickupPointArr[x].longitude);
            xsw.writeEndElement();
            xsw.writeStartElement('custom-attributes');
            xsw.writeStartElement('custom-attribute');
            xsw.writeAttribute('attribute-id', 'storeType');
            xsw.writeCharacters('BRING_PICKUP_POINT');
            xsw.writeEndElement();
            xsw.writeEndElement(); // custom-attributes
            xsw.writeEndElement(); // store
        }
        xsw.writeEndElement(); // stores
        xsw.close();
        fileWriter.close();
    } catch (e) {
        return new Status(Status.ERROR, 'ERROR', e.message);
    }
    return new Status(Status.OK);
}

/**
 * Entry function for the job
 * @param {Object} params - job parameters
 * @returns {dw.system.Status} - Status of job work
 */
function execute(params) {
    var StringUtils = require('dw/util/StringUtils');
    var Calendar = require('dw/util/Calendar');
    var Logger = require('dw/system/Logger').getLogger('bringPickupPointJob', 'bringPickupPointJob');
    var File = require('dw/io/File');
    var Site = require('dw/system/Site');
    var SiteID = Site.getCurrent().ID;

    var workingFolder = params.workingFolder;
    var timeStamp = '_' + StringUtils.formatCalendar(new Calendar(), 'yyyMMddHHmmss');
    var fileXMLName = workingFolder + File.SEPARATOR + 'bringPickupPoint_' + SiteID + timeStamp + '.xml';
    var fileXML = new File(File.IMPEX + File.SEPARATOR + fileXMLName);
    if (!fileXML.exists()) {
        fileXML.createNewFile();
    }

    var pickupPointArr = getPickupPoint();
    var status = createStoresImpexFile(pickupPointArr, fileXML);

    if (status.error) {
        fileXML.remove();
        Logger.debug('Error during creating of pickup point xml. Error Message: {0}', status.getMessage());
        return new Status(Status.ERROR);
    }
    return new Status(Status.OK);
}

module.exports = {
    execute: execute
};
