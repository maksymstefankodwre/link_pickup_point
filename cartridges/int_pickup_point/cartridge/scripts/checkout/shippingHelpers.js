'use strict';

var base = module.superModule;

/**
 * Mark a shipment to be picked up from point
 * @param {dw.order.Shipment} shipment - line item container to be marked for pickup point
 * @param {string} pickupPointId - Id of the pickup point for shipment to be picked up from.
 */
function markShipmentForPickupPoint(shipment, pickupPointId) {
    var Transaction = require('dw/system/Transaction');
    Transaction.wrap(function () {
        shipment.custom.bringPickupPointID = pickupPointId; // eslint-disable-line no-param-reassign
    });
}

/**
 * Remove pickup instore indicators from the shipment
 * @param {dw.order.Shipment} shipment - Shipment to be marked
 */
function markShipmentForShipping(shipment) {
    var Transaction = require('dw/system/Transaction');
    Transaction.wrap(function () {
        shipment.custom.bringPickupPointID = null; // eslint-disable-line no-param-reassign
    });
}

module.exports = {
    markShipmentForPickupPoint: markShipmentForPickupPoint,
    markShipmentForShipping: markShipmentForShipping
};
Object.keys(base).forEach(function (prop) {
    // eslint-disable-next-line no-prototype-builtins
    if (!module.exports.hasOwnProperty(prop)) {
        module.exports[prop] = base[prop];
    }
});
