'use strict';

var base = module.superModule;
var collections = require('*/cartridge/scripts/util/collections');

/**
 * Loop through all shipments and make sure all not null
 * @param {dw.order.LineItemCtnr} lineItemContainer - Current users's basket
 * @returns {boolean} - allValid
 */
function ensureValidShipments(lineItemContainer) {
    var shipments = lineItemContainer.shipments;
    var pickupPointAddress = true;
    var allValid = collections.every(shipments, function (shipment) {
        if (shipment) {
            var hasPickupPointID = shipment.custom && shipment.custom.bringPickupPointID;
            if (shipment.shippingMethod.custom && shipment.shippingMethod.custom.isBringPickupPoint && !hasPickupPointID) {
                pickupPointAddress = false;
            }
            var address = shipment.shippingAddress;
            return address && address.address1 && pickupPointAddress;
        }
        return false;
    });
    return allValid;
}

module.exports = {
    ensureValidShipments: ensureValidShipments
};
Object.keys(base).forEach(function (prop) {
    // eslint-disable-next-line no-prototype-builtins
    if (!module.exports.hasOwnProperty(prop)) {
        module.exports[prop] = base[prop];
    }
});
