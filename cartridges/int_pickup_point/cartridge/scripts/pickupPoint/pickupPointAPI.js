'use strict';

var Site = require('dw/system/Site');
var PickupPointResponseModel = require('*/cartridge/models/pickupPointAPIResponse');
var pickupPointsService = require('*/cartridge/scripts/services/pickupPointService');
var currentSite = Site.current;
var baseServiceUrl = pickupPointsService.getURL() + '/' + currentSite.getCustomPreferenceValue('bringPickupPointCountry');

/**
 * Searchs for pickup points by provided postal code
 * @param {string} postalCode - postalCode
 * @returns {Array} an array of pickup points
 */
function searchByPostalCode(postalCode) {
    pickupPointsService.setURL(baseServiceUrl + '/postalCode/' + postalCode + '.json');
    var serviceResult = pickupPointsService.call();
    if (serviceResult.ok) {
        return new PickupPointResponseModel(serviceResult.object).pickupPoints;
    }
    // TODO: Logger.error(...);
    return [];
}

/**
 * Returns pickup point object if found by id
 * @param {string} id - pickup point id
 * @returns {Object} pickup point object
 */
function getPickupPointByID(id) {
    pickupPointsService.setURL(baseServiceUrl + '/id/' + id + '.json');
    var serviceResult = pickupPointsService.call();
    if (serviceResult.ok) {
        return new PickupPointResponseModel(serviceResult.object).pickupPoints[0] || null;
    }
    // TODO: Logger.error(...);
    return null;
}

/**
 * Returns all pickup points available for current site
 * @returns {Array} an array of pickup points
 */
function getAllPickupPoint() {
    pickupPointsService.setURL(baseServiceUrl + '/all.json');
    var serviceResult = pickupPointsService.call();
    if (serviceResult.ok) {
        return new PickupPointResponseModel(serviceResult.object).pickupPoints;
    }
    // TODO: Logger.error(...);
    return [];
}

module.exports = {
    searchByPostalCode: searchByPostalCode,
    getPickupPointByID: getPickupPointByID,
    getAllPickupPoint: getAllPickupPoint
};
