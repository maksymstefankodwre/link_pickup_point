'use strict';

var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');

/**
 * @description creating of the ServiceDefinition object for the service
 */
var pickupPointService = LocalServiceRegistry.createService('pickup.point', {
    createRequest: function (svc, args) {
        svc.setRequestMethod('GET');
        return args;
    },
    parseResponse: function (svc, client) {
        return JSON.parse(client.text);
    }
});

module.exports = pickupPointService;
