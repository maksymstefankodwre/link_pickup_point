'use strict';

/**
 * Show pickup point block when appropriate shipping method is selected
 * @param {Object} shippingForm - DOM element that contains current shipping form
 */
function showPickupPoint(shippingForm) {
    // hide address panel
    shippingForm.find('.shipment-selector-block').addClass('d-none');
    shippingForm.find('.shipping-address-block').addClass('d-none');
    shippingForm.find('.pickup-point-selected-block').addClass('d-none');

    shippingForm.find('.gift-message-block').addClass('d-none');
    shippingForm.find('.gift').prop('checked', false);
    shippingForm.find('.gift-message').addClass('d-none');

    shippingForm.find('.pickup-point-block').removeClass('d-none');
    shippingForm.find('.pickup-point-selected-block').addClass('d-none');
    shippingForm.find('.pickup-point-search-form').removeClass('d-none');
    shippingForm.find('.pickup-point-results').html('');
    shippingForm.find('.select-pickup-point').removeClass('d-none').prop('disabled', true);
    shippingForm.find('input[name="pickupPointID"]').remove();
    shippingForm.find('.pickup-point-selected-block .pickup-point-details').remove();
}

/**
 * Hide pickup point block and restore address form
 * @param {Object} shippingForm - DOM element with current form
 * @param {Object} data - data containing customer and order objects
 */
function hidePickupPoint(shippingForm, data) {
    if (data.order.usingMultiShipping) {
        $('body').trigger('pickuppoint:hideMultiPickupPoint', {
            form: shippingForm,
            customer: data.customer,
            order: data.order
        });
    } else {
        $('body').trigger('pickuppoint:hideSinglePickupPoint', {
            form: shippingForm,
            customer: data.customer,
            order: data.order
        });
    }

    shippingForm.find('.pickup-point-block').addClass('d-none');
    shippingForm.find('.gift-message-block').removeClass('d-none');
    shippingForm.find('.pickup-point-selected-block').addClass('d-none');
    shippingForm.find('input[name="pickupPointID"]').remove();
    shippingForm.find('.pickup-point-details').remove();
}

/**
 * Shows selected pickup point details
 * @param {Object} shippingForm - DOM element with current form
 * @param {string} pickupPointID - selected pickup point
 */
function showPickupPointSelected(shippingForm, pickupPointID) {
    shippingForm
            .find('.pickup-point-block')
            .removeClass('d-none')
            .append('<input type="hidden" name="pickupPointID" value="' + pickupPointID + '" />');

    shippingForm.find('.pickup-point-selected-block').removeClass('d-none');
    shippingForm.find('.shipping-address-block').addClass('d-none');
    shippingForm.find('.pickup-point-search-form').addClass('d-none');
    shippingForm.find('.select-pickup-point').addClass('d-none');
}

/**
 * Handles the initial state of single shipping on page load
 */
function handleInitialSingleship() {
    var pickupPointMethodSelected = $(':checked', '.shipping-method-list').data('pickup-point');
    var pickupPointSelected = $('.pickup-point-details').length;
    var shippingForm = $('.single-shipping .shipping-form');
    var pickupPointID = pickupPointSelected ? $('.pickup-point-details').data('pickup-point-id') : null;

    if (pickupPointMethodSelected && !pickupPointSelected) {
        showPickupPoint(shippingForm);
    } else if (pickupPointMethodSelected && pickupPointSelected) {
        showPickupPointSelected(shippingForm, pickupPointID);
    }
}

/**
 * Handles the initial state of multi-shipping on page load
 */
function handleInitialMultiship() {
    $(':checked', '.multi-shipping .shipping-method-list').each(function () {
        var pickupPointMethodSelected = $(this).data('pickup-point');
        var shippingForm = $(this).closest('form');
        var pickupPoint = shippingForm.find('.pickup-point-details');
        var pickupPointSelected = pickupPoint.length;
        var pickupPointID = pickupPointSelected ? pickupPoint.data('pickup-point-id') : null;

        if (pickupPointMethodSelected && !pickupPointSelected) {
            showPickupPoint(shippingForm);
        } else if (pickupPointMethodSelected && pickupPointSelected) {
            showPickupPointSelected(shippingForm, pickupPointID)
        } else {
            shippingForm.find('.pickup-point-block').addClass('d-none');
            shippingForm.find('.shipping-address-block').removeClass('d-none');
        }
    });
}

/**
 * Search for pickup points with postal code
 * @param {HTMLElement} element - the target html element
 * @returns {boolean} false to prevent default event
 */
function search(element) {
    var $form = element.closest('.pickup-point-search-form');
    var url = $form.attr('action');
    url = url + '?postalCode=' + $form.find('[name="postalCode"]').val();
    $.ajax({
        url: url,
        type: $form.attr('method'),
        success: function (response) {
            $('.pickup-point-results').removeClass('d-none')
                .html(response.pickupPointsResultsHtml).css({ maxHeight: '500px', overflow: 'scroll' });
        }
    });
    return false;
}

module.exports = {
    watchForPickupPointShipping: function () {
        $('body').on('checkout:updateCheckoutView', function (e, data) {
            if (!data.urlParams || !data.urlParams.shipmentUUID) {
                data.order.shipping.forEach(function (shipment) {
                    var form = $('.shipping-form input[name="shipmentUUID"][value="' + shipment.UUID + '"]').closest('form');

                    if (shipment.selectedShippingMethod.pickupPointEnabled) {
                        showPickupPoint(form);
                    } else {
                        hidePickupPoint(form, data);
                    }
                });

                return;
            }

            var shipment = data.order.shipping.find(function (s) {
                return s.UUID === data.urlParams.shipmentUUID;
            });

            var shippingForm = $('.shipping-form input[name="shipmentUUID"][value="' + shipment.UUID + '"]').closest('form');
            if (shipment.selectedShippingMethod.pickupPointEnabled) {
                showPickupPoint(shippingForm);
            } else {
                hidePickupPoint(shippingForm, data);
            }
        });
    },
    selectPickupPoint: function () {
        $('.pickup-point-block').on('click', '.select-pickup-point', (function (e) {
            e.preventDefault();
            var selectedPoint = $(':checked', '.pickup-point-results');
            var data = {
                pickupPointID: selectedPoint.val(),
                pickupPointDetailsHtml: selectedPoint.siblings('label').find('.pickup-point-details').html(),
                event: e
            };

            $('body').trigger('pickuppoint:selected', data);
        }));
    },
    updateSelectPickupPointButton: function () {
        $('body').on('change', '.select-pickup-point-input', (function () {
            $('.select-pickup-point').prop('disabled', false);
        }));
    },
    watchForPickupPointSelection: function () {
        $('body').on('pickuppoint:selected', function (e, data) {
            var pickupPointPanel = $(data.event.target).parents('.pickup-point-block');
            var newLabel = $(data.pickupPointDetailsHtml);
            var content = $('<div class="pickup-point-details"></div>').append(newLabel);

            pickupPointPanel.find('.pickup-point-selected-block').append(content);
            pickupPointPanel.append('<input type="hidden" name="pickupPointID" value="' + data.pickupPointID + '" />');
            pickupPointPanel.find('.pickup-point-selected-block').removeClass('d-none');
            pickupPointPanel.find('.pickup-point-results').addClass('d-none');
            pickupPointPanel.find('.pickup-point-search-form').addClass('d-none');
            pickupPointPanel.find('.select-pickup-point').addClass('d-none');
        });
    },
    initialPickupPointMethodSelected: function () {
        $(document).ready(function () {
            var isMultiship = $('#checkout-main').hasClass('multi-ship');
            if (isMultiship) {
                handleInitialMultiship();
            } else {
                handleInitialSingleship();
            }
        });
    },
    changePickupPoint: function () {
        $('body').on('click', '.change-pickuppoint', (function () {
            showPickupPoint($(this).closest('form'));
        }));
    },
    updateAddressButtonClick: function () {
        $('body').on('click', '.btn-show-details', (function () {
            $(this).closest('.shipment-selector-block').siblings('.shipping-address-block').removeClass('d-none');
        }));
    },
    hideMultiPickupPoint: function () {
        $('body').on('pickuppoint:hideMultiPickupPoint', function (e, data) {
            data.form.find('.shipping-address-block').removeClass('d-none');
            data.form.find('.shipment-selector-block').removeClass('d-none');

            if (!data.customer.registeredUser) {
                data.form.attr('data-address-mode', 'new');
            } else {
                data.form.attr('data-address-mode', 'edit');
            }
        });
    },
    hideSinglePickupPoint: function () {
        $('body').on('pickuppoint:hideSinglePickupPoint', function (e, data) {
            if (data.customer.registeredUser) {
                if (data.customer.addresses.length) {
                    data.form.find('.shipment-selector-block').removeClass('d-none');
                    if (!data.order.shipping[0].matchingAddressId) {
                        data.form.find('.shipping-address-block').removeClass('d-none');
                    } else {
                        data.form.attr('data-address-mode', 'edit');

                        var addressSelectorDropDown = data.form.find('.addressSelector option[value="ab_' + data.order.shipping[0].matchingAddressId + '"]');
                        $(addressSelectorDropDown).prop('selected', true);
                    }
                } else {
                    data.form.find('.shipping-address-block').removeClass('d-none');
                }
            } else {
                data.form.find('.shipping-address-block').removeClass('d-none');
                data.form.find('.shipment-selector-block').removeClass('d-none');
            }
        });
    },
    actionEditMultiShip: function () {
        $('body').on('shipping:editMultiShipAddress', function (e, data) {
            var shippingForm = data.form;
            var pickupPointSelected = shippingForm.find(':checked', '.shipping-method-list').data('pickup-point');
            if (pickupPointSelected) {
                showPickupPoint(shippingForm);
            }
        });
    },
    search: function () {
        $('.pickup-point-block .btn-pickuppoint-search[type="button"]').click(function (e) {
            e.preventDefault();
            search($(this));
        });
    }
};
